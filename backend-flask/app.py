from flask import Flask, request, jsonify
from werkzeug import secure_filename
import os
import csv
import numpy as np

ALLOWED_EXTENSIONS = set(['txt', 'mp3'])

app = Flask(__name__)

def text_read(text_file_path, height=200, width=576, channel=1, separator=","):
    content = []
    try:
        with open(text_file_path) as file:
            lines = file.readlines()
            for line in lines:
                try:
                    numbers = [int(character) for character in line.split(separator)[:-1]]
                except ValueError:
                    numbers = [float(character) for character in line.split(separator)[:-1]]
                content.append(numbers)
            content = np.array(content)
            [h, w] = np.shape(content)
            height_new = None if h < height else height
            width_new = None if w < width else width
            if channel == 0:
                content = content[:height_new, :width_new]
            else:
                content = np.reshape(content, [h, w, channel])
                content = content[:height_new, :width_new, :channel]
    except ValueError:
        print("Error read: %s" % text_file_path)
    return content

def qmdct_extractor(mp3_file_path, width=576, frame_num=50, coeff_num=576):
    wav_file_path = mp3_file_path.replace(".mp3", ".wav")
    txt_file_path = mp3_file_path.replace(".mp3", ".txt")
    command = "wine /home/rizkiduwinanto/Documents/TRISKY/Eksperimen/python_scripts/tools/lame_qmdct.exe " + mp3_file_path + " -framenum " + str(frame_num) + " -startind 0 " + " -coeffnum " + str(coeff_num) + " --decode"
    os.system(command)
    height = frame_num * 4
    content = text_read(text_file_path=txt_file_path, height=height, width=width)
    os.remove(txt_file_path)
    os.remove(wav_file_path)
    return content

@app.route('/steganalysis', methods=['POST'])
def steganalysis():
    if request.method == 'POST':
      try:
        f = request.files['file']
        ext = f.filename.split('.')[-1]
        f.save(secure_filename(f.filename))
        if ext == 'txt':
          QMDCT = text_read(f.filename)
        elif ext == 'mp3':
          QMDCT = qmdct_extractor(f.filename)
        else:
          return jsonify("Error")

        height, width, channel = 200, 576, 1
        carrier = 'qmdct'

        steganalysis_file_path = secure_filename(f.filename)

        algorithm_pred = 'HCM-EECS'
        len_pred = 100
        print('Algorithm : {}'.format(algorithm_pred))
        print('Length : {}'.format(len_pred))

      except ValueError:
        return jsonify("Error")
    return jsonify(algorithm=algorithm_pred, length=len_pred)

if __name__ == '__main__':
  app.run(debug=True,host='0.0.0.0')
