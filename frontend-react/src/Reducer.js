const resultReducer = (state, action) => {
  switch(action.type) {
    case 'ADD_RESULT':
      let newResult = {
        filename : action.filename,
        algorithm : action.algorithm,
        length : action.length
      }
      console.log(newResult)
      return newResult
    default:
      return state
  }
}


export default resultReducer;