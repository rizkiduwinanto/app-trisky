import React from 'react';
import UploadFile from './UploadFile';
import Result from './Result';

function App() {
  return (
    <div className="App">
      <div style={{textAlign: "center", marginTop: '20px'}}>
        <h1 className="display-5">Steganalysis App</h1>
      </div>
      <div className="container">
        <UploadFile />
      </div>
      <div className="container">
        <Result />
      </div>
    </div>
  );
}

export default App;
