
import React, { Component } from 'react'
import { connect } from 'react-redux';

class resultFile extends Component {
  render() {
    console.log(this.props.result)
    return (
      <div style={{overflowX:'auto', background:'silver', marginTop:'20px', padding:'20px', height:'50vh'}}>
        <h5>{this.props.result.filename ? 'Result of Steganalysis of :' + this.props.result.filename : 'Upload Files'}</h5>
        <div className="row">
          <div className="col-sm">
            <h2>Method: </h2>
            <h1>{this.props.result.algorithm ? this.props.result.algorithm : 'Not Defined'}</h1>
          </div>
          <div className="col-sm">
            <h2>Length: </h2>
            <h1>{this.props.result.length ? this.props.result.length : 'Not Defined'}</h1>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    result: state,
  };
}

export default connect(mapStateToProps)(resultFile);