import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addFile } from './Action';

class UploadFile extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
    this.fileInput = React.createRef();

  }

  handleChange(event) {
    event.preventDefault();
    this.props.addFile(this.fileInput.current.files[0]);
  }

  render() {
    return (
      <div className="input-group" style={{marginTop: '30px'}}>
        <div className="input-group-prepend">
          <span className="input-group-text" id="inputGroupFileAddon01">
            Upload
          </span>
        </div>
        <div className="custom-file">
          <input
            type="file"
            className="custom-file-input"
            id="inputGroupFile01"
            aria-describedby="inputGroupFileAddon01"
            ref={this.fileInput}
            onChange = {(e) => this.handleChange(e)}
          />
          <label className="custom-file-label" htmlFor="inputGroupFile01">
            Choose MP3/QMDCT file
          </label>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addFile: (file) => dispatch(addFile(file))
  }
}

export default connect(null, mapDispatchToProps)(UploadFile);