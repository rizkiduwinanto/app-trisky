export const addFile = (file) => async (dispatch, getState, url_api) => {
  try {
    const url = `${url_api}/steganalysis`;
    const formData = new FormData();
    formData.append('file', file);
    const response = await fetch(url, {
      method: 'POST',
      body: formData
    });
    const responseBody = await response.json();
    dispatch(addResult(file.name, responseBody));
  } catch (error) {
    console.log(error);
  }
} 

export function addResult(filename, responseBody) {
  return {
    type: 'ADD_RESULT',
    filename : filename,
    algorithm : responseBody.algorithm,
    length : responseBody.length
  };
}